//
//  View_List.m
//  connecttosqlserver
//
//  Created by Priyanka on 8/4/13.
//  Copyright (c) 2013 ikosmik. MIT License.
//

#import "UIListViewController.h"


@implementation UIListViewController

@synthesize listViewDelegate;
@synthesize listViewDataSource;
@synthesize tableViewList;
@synthesize nibsRegistered;
@synthesize arrayListItems;



#pragma mark - Table view data source

- (NSInteger) tableView : (UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    NSLog(@"numberOfRowsInSection");

    // Return the number of items in the list
    NSArray *array = [self.listViewDataSource itemsForList];
    if ((array != NULL) && (array != nil)) {
        NSLog(@"numberOfRowsInSection : %d", [self.listViewDataSource itemsForList].count);
        return [self.listViewDataSource itemsForList].count;
    }
    return 0;
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSLog(@"cellForRowAtIndexPath");

    static NSString *Cell_Identifier      = @"Cell_ListItem";
    static NSString *Cell_iPad_Identifier      = @"Cell_ListItem~ipad";
    Cell_ListItem *cell;
    UINib *cellNib;
    
    if(!self.nibsRegistered) {
        
        if ([UIListUtility getDeviceType] == kDevice_iPad) {
            NSLog(@"registering nib for iPad");
            cellNib = [UINib nibWithNibName:Cell_iPad_Identifier bundle:nil];
            [self.tableViewList registerNib:cellNib forCellReuseIdentifier:Cell_iPad_Identifier];
            
        } else {
            cellNib = [UINib nibWithNibName:Cell_Identifier bundle:nil];
            [self.tableViewList registerNib:cellNib forCellReuseIdentifier:Cell_Identifier];
        }
        
        self.nibsRegistered = YES;
    }
    
    NSUInteger row = [indexPath row];
    if ((self.arrayListItems == nil) || (self.arrayListItems == NULL)) {
        return nil;
    }
    NSString *listItemText = kStringEmpty;
    // is it a valid index?
    if (row < self.arrayListItems.count) {
        // is it an instance of NSString?
        if ([[self.arrayListItems objectAtIndex:row] isKindOfClass:[NSString class]]) {
                listItemText = (NSString *) [self.arrayListItems objectAtIndex:row];
        }
    }
    // @validate
    if ((listItemText == nil) || (listItemText == NULL)) {
        NSLog(@"ERROR : Invalid listitem at row %d", row);
        return nil;
    }
    
    // -------------- It is a plane, it is a bird ??? ; no! it's an iPad ;-) --------------
    if ([UIListUtility getDeviceType] == kDevice_iPad) {
        cell = [self.tableViewList dequeueReusableCellWithIdentifier:Cell_iPad_Identifier];
    }
    // -------------- not an iPad ? Now that we support only iPhone otherwise --------------
    else {
        cell = [self.tableViewList dequeueReusableCellWithIdentifier:Cell_Identifier];
    }
    [cell initializeWithListItem : listItemText];
    // is the list item height set?
    if ([self isListItemHeightExplicitlySet]) {
        [cell adjustListItemToHeight : [self.listViewDelegate heightForItem]];
    }
    // is the font set?
    if ([self.listViewDelegate respondsToSelector:@selector(fontForListItem)]) {
        [cell setListItemFont : [self.listViewDelegate fontForListItem]];
    }
    // Is the list-separator-color set?
    if ([self.listViewDelegate respondsToSelector:@selector(colorForListSeparator)]) {
        [cell setListSeparatorToColor : [self.listViewDelegate colorForListSeparator]];
    }

    return cell;
}


/*
// Override to support conditional editing of the table view.
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Return NO if you do not want the specified item to be editable.
    return YES;
}
*/

/*
// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        // Delete the row from the data source
        [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
    }   
    else if (editingStyle == UITableViewCellEditingStyleInsert) {
        // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
    }   
}
*/

/*
// Override to support rearranging the table view.
- (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath
{
}
*/

/*
// Override to support conditional rearranging of the table view.
- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Return NO if you do not want the item to be re-orderable.
    return YES;
}
*/



#pragma mark - Table view delegate

/**
 * We assume that a minimum list height is set.
 * If the user has explicitly set a value (via the delegate) more than the minimum height, then it is considered.
 */
-(CGFloat) tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    NSLog(@"heightForRowAtIndexPath");

    if ([self isListItemHeightExplicitlySet]) {
        return [self.listViewDelegate heightForItem];
    }
    return [self getDefaultListItemHeight];
}


- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    NSLog(@"didSelectRowAtIndexPath");
    Cell_ListItem *cell = (Cell_ListItem *) [self.tableViewList cellForRowAtIndexPath:indexPath];
    [cell processListItemSelection];
    if ([cell isListItemSelected]) {
        [arraySelectedItems addObject:[cell getListItem]];
        NSLog(@"Added %@ to selected-list", [cell getListItem]);
    } else {
        [arraySelectedItems removeObject:[cell getListItem]];
        NSLog(@"removed %@ from selected-list", [cell getListItem]);
    }
    self.listViewDataSource.arraySelectedList = [NSArray arrayWithArray : arraySelectedItems];
}



#pragma mark - Selection Retriever

/**
 * Returns the list of the items currently selected in the list
 */
-(NSArray *) getSelectedListItems {
    NSLog(@"getSelectedListItems : returning %d selected items", arraySelectedItems.count);
    return [NSArray arrayWithArray : arraySelectedItems];
}



#pragma mark - Helper Methods


/**
 * Checks if
 * 1. Is the list item height explicitly set in the delegate?
 *          1.a If yes, check if that is greater than the default value (based on the device type)
 *                      1.a.A If yes, return YES
 *                      1.a.B If not, return NO
 * @return      BOOL        YES, if the list-item-height is set & > default ; NO, otherwise.
 */
-(BOOL) isListItemHeightExplicitlySet {
    NSLog(@"isListItemHeightExplicitlySet");

    if ([self.listViewDelegate respondsToSelector:@selector(heightForItem)]) {
        
        CGFloat defaultHeight = [self getDefaultListItemHeight];
        CGFloat heightForItem = [self.listViewDelegate heightForItem];
        
        if (heightForItem > defaultHeight) {
            return YES;
        }
    }
    return NO;
}


/**
 * @return      CGFloat     the default list item height, based on the device type
 */
-(CGFloat) getDefaultListItemHeight {
    NSLog(@"getDefaultListItemHeight");

    CGFloat defaultHeight = kListItemHeightDefault_iPhone;
    
    if ([UIListUtility getDeviceType] == kDevice_iPad) {
        defaultHeight = kListItemHeightDefault_iPad;
    }
    return defaultHeight;
}



#pragma mark - Validation

/**
 * Does the dataSource instance conform to the UIListViewDataSource Protocol?
 * If not, raise an exception
 * If yes, set it to the class' dataSource attribute
 * @param       id<UIListViewDataSource>        the datasource instance
 */
-(void) setListViewDataSource : (id<UIListViewDataSource>) dataSource {
    NSLog(@"setListViewDataSource");

    if ( ! [dataSource conformsToProtocol:@protocol(UIListViewDataSource)]) {
        [NSException raise:@"UIListViewDataSource Exception"
                    format:@"Parameter does not conform to UIListViewDataSource protocol at line %d", (int)__LINE__];
    }
    self.listViewDataSource = dataSource;
}


/**
 * Does the delegate instance conform to the UIListViewDelegate Protocol?
 * If not, raise an exception
 * If yes, set it to the class' delegate attribute
 * @param       id<UIListViewDelegate>      the delegate instance
 */
-(void) setListViewDelegate : (id<UIListViewDelegate>) delegate {
    NSLog(@"setListViewDelegate");

    if ( ! [delegate conformsToProtocol:@protocol(UIListViewDelegate)]) {
        [NSException raise:@"UIListViewDelegate Exception"
                    format:@"Parameter does not conform to UIListViewDelegate protocol at line %d", (int)__LINE__];
    }
    self.listViewDelegate = delegate;
}



#pragma mark - Generic Class Methods

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
//- (id)initWithStyle:(UITableViewStyle)style
    //self = [super initWithStyle : UITableViewStyleGrouped];
    if (self) {
        // Custom initialization
    }
    return self;
}

-(void) viewWillAppear:(BOOL)animated {
        NSLog(@"UIListViewController :  viewWillAppear");
    self.arrayListItems = [self.listViewDataSource itemsForList];
    self.nibsRegistered = NO;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    NSLog(@"UIListViewController :  viewdidload");
    //self.listViewDelegate.viewList = self.view;
    
    self.tableViewList = [[UITableView alloc] initWithFrame:self.tableViewList.bounds style:UITableViewStyleGrouped];
    [self.tableViewList setBackgroundView:nil];
    [self.tableViewList setSeparatorColor:[UIColor whiteColor]];

    
    // Set the list title, if explicitly set in the delegate
    if ([self.listViewDelegate respondsToSelector:@selector(titleForList)]) {
        NSString *title = [self.listViewDelegate titleForList];
        if ((title != NULL) && (title != nil)) {
            [self.labelTitle setText : title];
        } else {
            [self.labelTitle setText:kStringEmpty];
        }
    } else {
        [self.labelTitle setText:kStringEmpty];
    }
    
    // Uncomment the following line to preserve selection between presentations.
    // self.clearsSelectionOnViewWillAppear = NO;
    
    // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
    // self.navigationItem.rightBarButtonItem = self.editButtonItem;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
