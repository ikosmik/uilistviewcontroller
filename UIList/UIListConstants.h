//
//  UIListConstants.h
//  connecttosqlserver
//
//  Created by Priyanka on 8/4/13.
//  Copyright (c) 2013 ikosmik. All rights reserved.
//

#import <Foundation/Foundation.h>


@interface UIListConstants : NSObject

FOUNDATION_EXPORT NSString * const kStringEmpty;

// List related
FOUNDATION_EXPORT CGFloat const kListItemHeightDefault_iPhone;
FOUNDATION_EXPORT CGFloat const kListItemHeightDefault_iPad;
FOUNDATION_EXPORT CGFloat const kListItemCheckboxHeight;

// Device - Types
FOUNDATION_EXPORT int const kDevice_iPhone;
FOUNDATION_EXPORT int const kDevice_iPhone5;
FOUNDATION_EXPORT int const kDevice_iPad;

// Device -Screen height
FOUNDATION_EXPORT int const kiPhoneScreenHt;
FOUNDATION_EXPORT int const kiPhone5ScreenHt;

// Images
FOUNDATION_EXPORT NSString * const kImg_Tickmark_Grey;
FOUNDATION_EXPORT NSString * const kImg_Tickmark_Green;

@end
