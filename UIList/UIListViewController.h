//
//  View_List.h
//  connecttosqlserver
//
//  Created by Priyanka on 8/4/13.
//  Copyright (c) 2013 ikosmik. MIT License.
//

#import <UIKit/UIKit.h>
#import "Cell_ListItem.h"
#import "UIListUtility.h"

@class Cell_ListItem;


/**
 * Protocol that acts as the delegate for the list instance
 */
@protocol UIListViewDelegate <NSObject>

    @optional
            // height of the row
            -(CGFloat) heightForItem;
            // is it multi-selection or single selection list
            // -(BOOL) allowMultipleSelection;
            // title for the list
            -(NSString *) titleForList;
            // font for list
            -(UIFont *) fontForListItem;
            // color for separator
            -(UIColor *) colorForListSeparator;

@end


/**
 * Protocol that provides as the data source for the list instance
 */
@protocol UIListViewDataSource <NSObject>

// The list of currently selected items in the list
@property (nonatomic, strong) NSArray *arraySelectedList;

@required
        // array of items to be displayed in the list
        -(NSArray *) itemsForList;

@end



@interface UIListViewController : UIViewController <UITableViewDataSource, UITableViewDelegate> {
    NSMutableArray *arraySelectedItems;
}

@property (nonatomic, weak) id <UIListViewDelegate> listViewDelegate;
@property (nonatomic, weak) id <UIListViewDataSource> listViewDataSource;
@property (nonatomic, strong) IBOutlet UILabel *labelTitle;
@property (nonatomic, strong) IBOutlet UIView *headerView;
@property (nonatomic, strong) IBOutlet NSString *title;
@property (nonatomic, strong) IBOutlet UITableView *tableViewList;
@property (nonatomic) BOOL nibsRegistered;
@property (nonatomic, strong) NSArray *arrayListItems;

-(NSArray *) getSelectedListItems;

@end
