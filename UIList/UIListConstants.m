//
//  UIListConstants.m
//  connecttosqlserver
//
//  Created by Priyanka on 8/4/13.
//  Copyright (c) 2013 ikosmik. All rights reserved.
//

#import "UIListConstants.h"

@implementation UIListConstants

NSString * const kStringEmpty = @"";

// List related
CGFloat const kListItemHeightDefault_iPhone = 100.0;
CGFloat const kListItemHeightDefault_iPad = 150.0;
CGFloat const kListItemCheckboxHeight = 50.0;

// Device - Types
int const kDevice_iPhone = 0;
int const kDevice_iPhone5 = 1;
int const kDevice_iPad = 2;

// Device -Screen height
int const kiPhoneScreenHt = @"480.0";
int const kiPhone5ScreenHt = @"568.0";

// Images
NSString * const kImg_Tickmark_Grey = @"icon_tickmark_lightgrey.png";
NSString * const kImg_Tickmark_Green = @"icon_tickmark_green.png";

@end
