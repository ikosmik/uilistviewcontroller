//
//  Cell_ListItem.h
//  connecttosqlserver
//
//  Created by Priyanka on 8/4/13.
//  Copyright (c) 2013 ikosmik. MIT License.
//

#import <UIKit/UIKit.h>
#import "UIListConstants.h"
#import "UIListUtility.h"


@interface Cell_ListItem : UITableViewCell

@property (nonatomic, strong) IBOutlet UILabel *labelItem;
@property (nonatomic, strong) IBOutlet UIImageView *imgSeparator;
@property (nonatomic, strong) IBOutlet UIImageView *imgCheckbox;
@property (nonatomic)  BOOL isSelected;

-(void) initializeWithListItem : (NSString *) listItem;
-(void) adjustListItemToHeight : (CGFloat) cellHeight;
-(void) processListItemSelection;
-(BOOL) isListItemSelected;
-(NSString *) getListItem;
-(void) setListItemFont : (UIFont *) font;
-(void) setListSeparatorToColor : (UIColor *) color;

@end
