//
//  UIListUtility.h
//  connecttosqlserver
//
//  Created by Priyanka on 8/4/13.
//  Copyright (c) 2013 ikosmik. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "UIListConstants.h"

@interface UIListUtility : NSObject

+(int) getDeviceType;

@end
