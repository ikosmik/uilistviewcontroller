//
//  Cell_ListItem.m
//  connecttosqlserver
//
//  Created by Priyanka on 8/4/13.
//  Copyright (c) 2013 iKosmik. MIT License.
//

#import "Cell_ListItem.h"

@implementation Cell_ListItem

@synthesize labelItem;
@synthesize imgSeparator;
@synthesize imgCheckbox;
@synthesize isSelected;



#pragma mark - Initialization

/**
 * Initializes the list item with the text passed in
 * @param       NSString        the text to be set for the list item
 */
-(void) initializeWithListItem : (NSString *) listItem {

    // Validate the list item string
    if ((listItem != NULL) && (listItem != nil)) {
        [self.labelItem setText:listItem];
        
    } else {
        [self.labelItem setText:kStringEmpty];
    }
    [self.imgCheckbox setImage:[UIImage imageNamed:kImg_Tickmark_Grey]];
    self.isSelected = NO;
}



#pragma mark - Selection Handler

/**
 * Change the state of the list item
 *          - Select it, if not yet selected
 *          - Deselect it, if already selected
 */
-(void) processListItemSelection {
    
    if (self.isSelected) {
        [self.imgCheckbox setImage:[UIImage imageNamed:kImg_Tickmark_Grey]];
    } else {
        [self.imgCheckbox setImage:[UIImage imageNamed:kImg_Tickmark_Green]];
    }
    self.isSelected = ! (self.isSelected);
}



#pragma mark - UI Related Methods

/**
 * Sets the font for the text in the list
 * @param       UIFont      font for text in the list
 */
-(void) setListItemFont : (UIFont *) font {
    if ((font != nil) && (font != NULL)) {
        [self.labelItem setFont:font];
    }
}


/**
 * Sets the color of the list item separator
 * @param       UIColor     color for the separator
 */
-(void) setListSeparatorToColor : (UIColor *) color {
    if ((color != nil) && (color != NULL)) {
        [self.imgSeparator setBackgroundColor:color];
    }
}



#pragma mark - Helper Methods

/**
 * Is the list item selected?
 * @return      BOOL        YES, if the list item's selected ; NO, if not
 */
-(BOOL) isListItemSelected {
    return self.isSelected;
}


/**
 * Gets the text in the list item
 * @return      NSString        the text in the list item
 */
-(NSString *) getListItem {
    return self.labelItem.text;
}


/**
 * We assume that this method is called if the user explicilty sets a height for the list item
 * which is greater than the assumed default height
 * This will adjus the UI components in the list item as per the given height
 * @param       CGFloat      height to which to adjust the UI Components to
 */
-(void) adjustListItemToHeight:(CGFloat)height {
    
    // ~~~~~~~~~~ For the text ~~~~~~~~~~
    CGFloat padding = 15.0;
    if ([UIListUtility getDeviceType] == kDevice_iPad) {
        padding = 50.0;
    }
    [self.labelItem setFrame:CGRectMake(self.labelItem.bounds.origin.x, padding/2.0,
                                        self.labelItem.bounds.size.width, height - padding)];
    
    // ~~~~~~~~~~ For the checkbox ~~~~~~~~~~
    [self.imgCheckbox setFrame:CGRectMake(
                                          self.imgCheckbox.bounds.origin.x, (height-kListItemCheckboxHeight)/2.0,
                                          self.imgCheckbox.bounds.size.width, self.imgCheckbox.bounds.size.height)];
}



#pragma mark - Generic Class Methods

- (id) initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
    }
    return self;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
