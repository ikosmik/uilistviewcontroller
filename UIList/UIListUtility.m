//
//  UIListUtility.m
//  connecttosqlserver
//
//  Created by Priyanka on 8/4/13.
//  Copyright (c) 2013 ikosmik. All rights reserved.
//

#import "UIListUtility.h"

@implementation UIListUtility

/**
 * Returns the device type
 *     - iPhone
 *     - iPhone5
 *     - iPad
 * @return      int     Returns the device type
 */
+(int) getDeviceType {
    
    if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPhone) {
        
        //  iPhone Screen Size
        if ([[UIScreen mainScreen] bounds].size.height == kiPhoneScreenHt) {
            return kDevice_iPhone;
        }
        //  iPhone5 Screen Size
        else if ([[UIScreen mainScreen] bounds].size.height == kiPhone5ScreenHt) {
            return kDevice_iPhone5;
        }
    }
    // iPad
    else if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPad) {
        return kDevice_iPad;
    }
    return kDevice_iPhone;
}

@end
