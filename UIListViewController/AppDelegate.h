//
//  AppDelegate.h
//  UIListViewController
//
//  Created by Priyanka on 8/5/13.
//
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
